$(function () {

    $.when(

        $.get("img/frontpage-animations/introduction.svg", function (svg) {
            $("#introduction_container").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/chapter1.svg", function (svg) {
            $("#chapter1_container").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch3-ship.svg", function (svg) {
            $("#ship").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch3-iceberg1.svg", function (svg) {
            $("#iceberg1").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch3-iceberg2.svg", function (svg) {
            $("#iceberg2").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch3-iceberg3.svg", function (svg) {
            $("#iceberg3").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch3-iceberg4.svg", function (svg) {
            $("#iceberg4").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/chapter4.svg", function (svg) {
            $("#chapter4-illustration").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch5-icon1.svg", function (svg) {
            $("#ch5-icon1").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch5-icon2.svg", function (svg) {
            $("#ch5-icon2").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch5-icon3.svg", function (svg) {
            $("#ch5-icon3").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch5-icon4.svg", function (svg) {
            $("#ch5-icon4").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/chapter6.svg", function (svg) {
            $("#chapter6_container").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/ch7-map.svg", function (svg) {
            $("#chapter7-svg").append(svg.documentElement);
        }),
        $.get("img/frontpage-animations/chapter8.svg", function (svg) {
            $("#chapter8-icon").append(svg.documentElement);
        })

    ).then(initAnimation);

    function initAnimation() {
        var tw = TweenMax,
            tl = new TimelineMax(),
            tl2 = new TimelineMax(),
            tl3 = new TimelineMax(),
            tl4 = new TimelineMax(),
            tl5 = new TimelineMax(),
            tl6 = new TimelineMax(),
            tl7 = new TimelineMax(),
            tlch2 = new TimelineMax(),
            moveBack1 = function() {
                tw.to("#ch8-h6-hand>g", 1, {
                    transformOrigin: "0% 60%",
                    rotation: 0
                });
            },
            moveBack2 = function() {
                tw.to("#ch8-h7-hand>g", 1, {
                    transformOrigin: "80% 70%",
                    rotation: 0
                });
            },
            tl8 = new TimelineMax({delay: 5, repeat: 17, onComplete: moveBack1}),
            tl9 = new TimelineMax({delay: 6, repeat: 17, onComplete: moveBack2});
        // Frontpage animation
        if(sessionStorage.getItem('frontpageAnim') !== 'yes') {  // check if slide down flag was set
    sessionStorage.setItem('frontpageAnim', 'yes');      // set the slide down flag
        tl.set("#anim-frontpage .btn", {
            transition: "none"
        })
            .from("#anim-frontpage", 4.3, {
                opacity: 0,
                delay: -0.2
            })
            .from("#anim-frontpage .plain-white", 1, {
                opacity: 0,
                delay: -1
            })
            .from("#frontpage-content h1", 1.3, {
                opacity: 0,
                y: -40,
                delay: -1,
                ease: Power3.easeInOut
            })
            .from("#frontpage-content h4", 1.3, {
                opacity: 0,
                y: -40,
                delay: -0.8,
                ease: Power3.easeInOut
            })
            .from("#frontpage-content .mockups img:first-child", 2, {
                opacity: 0,
                delay: -0.8,
                x: "-200%",
                ease: Power3.easeInOut
            })
            .staggerFrom(["#frontpage-content .mockups img:nth-child(2)",
                          "#frontpage-content .mockups img:nth-child(3)",
                          "#frontpage-content .mockups img:nth-child(4)",
                          "#frontpage-content .mockups img:nth-child(5)",
                          "#frontpage-content .mockups img:nth-child(6)",
                          "#frontpage-content .mockups img:nth-child(7)",
                          "#frontpage-content .mockups img:nth-child(8)",
                          "#frontpage-content .mockups img:nth-child(9)",
                         ], 1.3, {
                    opacity: 0,
                    x: "180%",
                    ease: Power3.easeInOut
                }, 0.25, "-=1")
            .from("#frontpage-content .mockups img:last-child", 2, {
                opacity: 0,
                x: 20,
                delay: -1,
                ease: Power3.easeInOut
            })
            .to(["#frontpage-content .mockups img:nth-child(2)",
                          "#frontpage-content .mockups img:nth-child(3)",
                          "#frontpage-content .mockups img:nth-child(4)",
                          "#frontpage-content .mockups img:nth-child(5)",
                          "#frontpage-content .mockups img:nth-child(6)",
                          "#frontpage-content .mockups img:nth-child(7)",
                          "#frontpage-content .mockups img:nth-child(8)",
                          "#frontpage-content .mockups img:nth-child(9)",
                         ], 1, {opacity:0, delay: -0.8})
            .staggerFrom(["#anim-frontpage .btn.down", "#anim-frontpage .btn.lightb"], 2, {
                opacity: 0,
                y: -30,
                ease: Power3.easeOut,
                onComplete: function () {
                    $("#anim-frontpage .btn").addClass("hover-transition");
                    tw.set("#anim-frontpage .btn", {
                        transition: ".25s ease"
                    });
                }
            }, 0.6, "-=3");     // perform the slide down
} 
        
        // Introduction animation
function animateIntroduction() {
        tl2.to("#introduction_container", 0.5, {opacity: 1})
            .from("#svg_intro #intr-illustr-layer1", 2, {
                transformOrigin: "50% 50%",
                scale: 0,
                opacity: 0,
                ease: Power4.easeInOut,
                delay: -0.4,
                onComplete: function () {
                    $("#svg_intro #intr-illustr-layer1").addClass("hover-scale1");
                }
            })
            .staggerFrom(["#intr-illustr-layer1>g:nth-child(1)",
                  "#intr-illustr-layer1>g:nth-child(2)",
                  "#intr-illustr-layer1>g:nth-child(3)",
                  "#intr-illustr-layer1>g:nth-child(4)",
                  "#intr-illustr-layer1>g:nth-child(5)",
                  "#intr-illustr-layer1>g:nth-child(6)",
                  "#intr-illustr-layer1>g:nth-child(7)"], 2, {
                transformOrigin: "50% 50%",
                scale: 0,
                opacity: 0,
                delay: -1.5,
                ease: Power4.easeOut
            }, 0.3)
            .from("#svg_intro #intr-illustr-layer3", 2, {
                transformOrigin: "50% 50%",
                scale: 2,
                opacity: 0,
                delay: -1,
                ease: Power4.easeOut
            })
            .staggerFrom(["#intr-illustr-layer3>g:nth-child(6)",
                  "#intr-illustr-layer3>g:nth-child(5)",
                  "#intr-illustr-layer3>g:nth-child(4)",
                  "#intr-illustr-layer3>g:nth-child(3)",
                  "#intr-illustr-layer3>g:nth-child(2)",
                  "#intr-illustr-layer3>g:nth-child(1)"], 2, {
                transformOrigin: "50% 50%",
                opacity: 0,
                ease: Power4.easeOut
            }, 0.2, "label-=2")
            .from("#svg_intro #intr-illustr-layer2", 2, {
                transformOrigin: "50% 50%",
                scale: 0,
                opacity: 0,
                ease: Power4.easeOut,
                delay: -2.5,
                onComplete: function () {
                    $("#svg_intro #intr-illustr-layer1>g").addClass("hover-scale2");
                    $("#svg_intro #intr-illustr-layer2").addClass("hover-scale1");
                    $("#svg_intro #intr-illustr-layer2>g").addClass("hover-scale2");
                }
            })
            .staggerFrom(["#intr-illustr-layer2>g:nth-child(1)",
                  "#intr-illustr-layer2>g:nth-child(2)",
                  "#intr-illustr-layer2>g:nth-child(3)",
                  "#intr-illustr-layer2>g:nth-child(4)",
                  "#intr-illustr-layer2>g:nth-child(5)",
                  "#intr-illustr-layer2>g:nth-child(6)",
                  "#intr-illustr-layer2>g:nth-child(7)"], 2, {
                transformOrigin: "50% 50%",
                opacity: 0,
                ease: Power4.easeOut
            }, 0.2, "-=2")
            .from("#svg_intro #intr-illustr-layer5", 2, {
                transformOrigin: "50% 50%",
                scale: 2,
                opacity: 0,
                delay: -2,
                ease: Power4.easeOut
            })
            .staggerFrom(["#intr-illustr-layer5>polygon:nth-child(6)",
                  "#intr-illustr-layer5>polygon:nth-child(5)",
                  "#intr-illustr-layer5>polygon:nth-child(4)",
                  "#intr-illustr-layer5>polygon:nth-child(3)",
                  "#intr-illustr-layer5>polygon:nth-child(2)",
                  "#intr-illustr-layer5>polygon:nth-child(1)"], 2, {
                transformOrigin: "50% 50%",
                opacity: 0,
                ease: Power4.easeOut
            }, 0.25, "-=2")
            .from("#svg_intro #intr-illustr-layer4", 2, {
                transformOrigin: "50% 50%",
                scale: 0,
                delay: -2.5,
                ease: Power4.easeOutб,
                onComplete: function () {
                    $("#svg_intro #intr-illustr-layer4").addClass("hover-scale1");
                }
            });
}       
function animateChapter1() {
    // Chapter 1 animation
        tl3.to("#chapter1_container", 0.5, {opacity: 1})
            .from("#svg_chapt1 #ch1-illustr-Circle_3", 2, {
                transformOrigin: "50% 50%",
                scale: 0,
                delay: -0.4,
                ease: Back.easeOut.config(1.7),
                onComplete: function () {
                    $("#svg_chapt1 #ch1-illustr-Circle_3").addClass("hover-scale1");
                }
            })
            .from("#circle3-1", 1, {
                transformOrigin: "50% 50%",
                scale: 0.6,
                opacity: 0,
                delay: -1.5
            })
            .from("#circle3-2", 1, {
                transformOrigin: "50% 50%",
                scale: 0.6,
                opacity: 0,
                delay: -1
            })
            .from("#svg_chapt1 #ch1-illustr-Circle_2", 3, {
                rotation: 140,
                transformOrigin: "50% 50%",
                scale: 0,
                delay: -1.5
            })
            .from("#svg_chapt1 #ch1-illustr-Circle_1", 5, {
                rotation: 90,
                transformOrigin: "50% 50%",
                scale: 0,
                delay: -2.5,
                onComplete: function () {
                    $("#ch1-illustr-Circle_1>g").addClass("hover-scale2");
                    $("#ch1-illustr-Circle_1").addClass("hover-scale1");
                }

            })
            .from("#ch1-illustr-white_lines", 1, {
                opacity: 0,
                delay: -1
            })
}
        
        function animateChapter2() {
            tw.to("#chapter2-illustration", 1, {opacity: 1});}
function animateChapter3() {
        // Chapter 3 animation
        tl4.set(["#iceberg1 .ice-text",
            "#iceberg2 .ice-text",
            "#iceberg3 .ice-text",
            "#iceberg4 .ice-text",
            "#iceberg1 svg .Layer_2",
            "#iceberg2 svg .Layer_2",
            "#iceberg3 svg .Layer_2",
            "#iceberg4 svg .Layer_2"], {
                opacity: 0
            })
            .to("#water-anim", 0.5, {opacity: 1})
            .from("#ship", 6, {
                transformOrigin: "top left",
                y: -50,
                scale: 0,
                ease: Power1.easeInOut
            })
            .from("#iceberg1", 4, {
                right: "10%",
                delay: -3.1,
                ease: Power1.easeOut,
                onComplete: function () {
                    tw.to(["#iceberg1 .ice-text", "#iceberg1 svg .Layer_2"], 2, {
                        opacity: 1
                    })
                    tw.to("#iceberg1>svg", 1.5, {
                        y: 4,
                        repeat: -1,
                        yoyo: true,
                        ease: Power1.easeInOut
                    })
                }
            })
            .from("#iceberg2", 4, {
                left: "10%",
                delay: -3.1,
                ease: Power1.easeOut,
                onComplete: function () {
                    tw.to(["#iceberg2 .ice-text", "#iceberg2 svg .Layer_2"], 2, {
                        opacity: 1
                    });
                    tw.to("#iceberg2>svg", 1.5, {
                        y: 4,
                        repeat: -1,
                        yoyo: true,
                        ease: Power1.easeInOut
                    });
                }
            })
            .from("#iceberg3", 4, {
                right: "10%",
                top: "60%",
                delay: -3.1,
                ease: Power1.easeOut,
                onComplete: function () {
                    tw.to(["#iceberg3 .ice-text", "#iceberg3 svg .Layer_2"], 2, {
                        opacity: 1
                    });
                    tw.to("#iceberg3>svg", 1.5, {
                        y: 4,
                        repeat: -1,
                        yoyo: true,
                        ease: Power1.easeInOut
                    });
                }
            })
            .from("#iceberg4", 4, {
                left: "10%",
                top: "60%",
                delay: -3.1,
                ease: Power1.easeOut,
                onComplete: function () {
                    tw.to(["#iceberg4 .ice-text", "#iceberg4 svg .Layer_2"], 2, {
                        opacity: 1
                    });
                    tw.to("#iceberg4>svg", 1.5, {
                        y: 4,
                        repeat: -1,
                        yoyo: true,
                        ease: Power1.easeInOut
                    });
                }
            })
}
function animateChapter4() {
        // Chapter 4 animation
        Draggable.create("#chapter4-illustration", {
            type: "scrollLeft",
            edgeResistance: 1,
            onDrag: Update,
            onThrowUpdate: Update
        });
        tw.set("#chapter4-illustration", {
            overflowX: 'hidden'
        });

        function Update() {
            var X = this.x,
                Y = this.y;
            tw.set('#ch4-bckg1', {
                x: X * -0.53
            });
            tw.set('#ch4-bckg2', {
                x: X * -0.5
            });
            tw.set('#ch4-bckg3', {
                x: X * -0.47
            });
            tw.set('#ch4-bckg4', {
                x: X * -0.43
            });
        };
}
function animateChapter5() {
        // Chapter 5 animation
        tl5.to("#chapter5-illustration", 0.5, {opacity: 1})
            .from("#ch5-i1-l5", 1, {
                x: -200,
                opacity: 0,
                delay: 1
            })
            .from(["#ch5-i1-l4","#ch5-i1-l3", "#ch5-i1-l2"], 2, {
                y: -300,
                ease: Elastic.easeOut.config(1.5, 1)
            })
            .from("#ch5-i1-l5", 1.8, {
                rotation: -10,
                delay: -1.8,
                ease: Elastic.easeOut.config(2, 0.75)
            })
            .from("#ch5-icon2+.ch5-icon-text", 1, {
            opacity: 0,
            delay: -2
        })
            .staggerFrom(["#ch5-i2-coins1>g:nth-child(1)",
                "#ch5-i2-coins1>g:nth-child(2)",
                "#ch5-i2-coins1>g:nth-child(3)",
                "#ch5-i2-coins1>g:nth-child(4)",
            ], 0.4, {
                opacity: 0,
                y: -20
            }, 0.1, "-=2")
            .from("#ch5-i2-woman", 0.5, {
                y: -50,
                opacity: 0,
                delay: -1,
                ease: Power2.easeIn
            })
            .staggerFrom(["#ch5-i2-coins2>g:nth-child(1)",
                "#ch5-i2-coins2>g:nth-child(2)",
                "#ch5-i2-coins2>g:nth-child(3)",
                "#ch5-i2-coins2>g:nth-child(4)",
                "#ch5-i2-coins2>g:nth-child(5)",
                "#ch5-i2-coins2>g:nth-child(6)",
                "#ch5-i2-coins2>g:nth-child(7)",
                "#ch5-i2-coins2>g:nth-child(8)"
            ], 0.4, {
                opacity: 0,
                y: -20
            }, 0.1, "-=2")
            .from("#ch5-i2-man", 1, {
                y: -50,
                opacity: 0,
                delay: -1,
                ease: Power2.easeIn,
                onComplete: function () {
                    tw.to("#ch5-i2-man-head", 1, {
                        transformOrigin: "bottom center",
                        rotation: 10,
                        delay: 1
                    });
                    tw.to("#ch5-i2-woman-head1", 1, {
                        transformOrigin: "bottom center",
                        rotation: -10,
                        delay: 1
                    });
                    tw.to("#ch5-i2-woman-head2", 1, {
                        transformOrigin: "50% 110%",
                        rotation: -10,
                        delay: 1
                    });
                    tw.to("#ch5-i2-woman-head3", 1, {
                        transformOrigin: "50% 130%",
                        rotation: -10,
                        delay: 1
                    });

                }
            });
        tl5.from("#ch5-icon3+.ch5-icon-text", 1, {
            opacity: 0
        })
            .from(["#ch5-i3-left_h1", "#ch5-i3-left_h2"], 1, {
                x: -30,
                opacity: 0,
                delay: -1,
                ease: Power3.easeIn
            })
            .from(["#ch5-i3-right_h1", "#ch5-i3-right_h2"], 1, {
                x: 30,
                opacity: 0,
                delay: -1,
                ease: Power3.easeIn
            })
            .to(["#ch5-i3-left_h1", "#ch5-i3-left_h2", "#ch5-i3-right_h1", "#ch5-i3-right_h2"], 0.5, {
                y: -5,
                ease: Power2.easeOut
            })
            .to(["#ch5-i3-left_h1", "#ch5-i3-left_h2", "#ch5-i3-right_h1", "#ch5-i3-right_h2"], 2, {
                y: 10,
                ease: Elastic.easeOut.config(1.5, 0.5),
                delay: -0.2
            })
            .to(["#ch5-i3-left_h1", "#ch5-i3-left_h2", "#ch5-i3-right_h1", "#ch5-i3-right_h2"], 0.5, {
                y: 0,
                delay: -1.5
            })
            .from("#ch5-icon4+.ch5-icon-text", 1, {
            opacity: 0,
            delay: -2
        })
            .from("#ch5-icon4 svg", 1, {
                x: -10,
                opacity: 0,
                delay: -2,
                onComplete: function() {
                    tw.fromTo("#ch5-i4_question", 0.5, {
                        scale: 0,
                        opacity: 0,
                        x: -20,
                        y: 20,
                        ease: Power3.easeOut
                    }, {scale: 1, opacity: 1, x: 0, y: 0});
                }
            })
            .to("#ch5-i4_rfoot", 0.7, {
                rotation: -15,
                repeat: 27,
                yoyo: true
            })
            .to("#ch5-i4_head", 3, {
                transformOrigin: "50% 100%",
                rotation: -10,
                repeat: 13,
                yoyo: true
            })
            .to("#ch5-i4_lefth", 2, {
                transformOrigin: "60% 0%",
                delay: 1.5,
                rotation: 4,
                repeat: 13,
                yoyo: true
            })
            .to("#ch5-i4_righth", 2, {
                transformOrigin: "40% 0%",
                delay: 1.5,
                rotation: -4,
                repeat: 13,
                yoyo: true
            });
}
function animateChapter6() {
    tl6.set("#svg_chapt6 #Layer_1", {
        transformOrigin: "55% 85%",
        rotation: -25
    })
    .to("#chapter6_container", 0.5, {opacity: 1})
        .from("#svg_chapt6 #Layer_6", 2,{
        transformOrigin: "50% 50%",
        scale: 0,
        opacity: 0,
        y: "-600%",
        ease: Power4.easeIn
    })
        .to("#svg_chapt6 #Layer_1", 3, {
        transformOrigin: "55% 85%",
        rotation: -15,
        delay: -0.1,
        ease: Elastic.easeOut.config(1.5, 0.5)
    })
    .from("#svg_chapt6 #Layer_5", 2,{
        transformOrigin: "50% 50%",
        scale: 0,
        opacity: 0,
        y: "-600%",
        ease: Power4.easeIn,
        delay: -3
    })
        .to("#svg_chapt6 #Layer_1", 3, {
        transformOrigin: "55% 85%",
        rotation: -7,
        delay: -1.1,
        ease: Elastic.easeOut.config(1.5, 0.5)
    })
    .from("#svg_chapt6 #Layer_4", 2,{
        transformOrigin: "50% 50%",
        scale: 0,
        opacity: 0,
        y: "-600%",
        ease: Power4.easeIn,
        delay: -3
    })
    .to("#svg_chapt6 #Layer_1", 3, {
        transformOrigin: "55% 85%",
        rotation: 0,
        delay: -1.1,
        ease: Elastic.easeOut.config(1.5, 0.5)
    })
    .to("#svg_chapt6 #Layer_1", 4, {
        transformOrigin: "55% 85%",
        rotation: 4,
        ease: Power1.easeInOut,
        repeat: 5,
        yoyo: true
    })
}
function animateChapter8() {
        tl7.to("#chapter8-icon", 0.5, {opacity: 1})
            .staggerFrom(["#ch8-bckg",
                        "#ch8-h1",
                        "#ch8-h2",
                        "#ch8-h4",
                        "#ch8-h5",
                        "#ch8-ladder1",
                        "#ch8-ladder2",
                        "#ch8-h3",
                        "#ch8-h6",
                        "#ch8-h7"], 1.5, {
            opacity: 0
        }, 0.5);
        tl8.from("#ch8-h6-hand", 1, {
            transformOrigin: "0% 60%",
            rotation: 43
        })
        .from("#ch8-h6-hand-stone", 1, {
            opacity: 0,
            delay: -1
        })
        .to("#ch8-h6-hand-stone", 0.5, {
            x: 5,
            y: -2,
            rotation: -5
        })
        .to("#ch8-h6-hand>g", 0.6, {
            transformOrigin: "0% 60%",
            rotation: 43
        })
        .to("#ch8-h6-hand>g", 0.6, {
            transformOrigin: "0% 60%",
            rotation: 0
        })
        .from("#ch8-h6-hand-stone2", 1, {
            opacity: 0,
            delay: -1
        })
        .to("#ch8-h6-hand-stone2", 0.5, {
            x: 39,
            y: -2,
            rotation: -5
        })
        .to(["#ch8-h6-hand-stone","#ch8-h6-hand-stone2"], 1, {
            opacity: 0
        })
        .to("#ch8-h6-hand>g", 0.6, {
            transformOrigin: "0% 60%",
            rotation: 43
        });
        tl9.from("#ch8-h7-hand", 1, {
            transformOrigin: "80% 70%",
            rotation: -90
        })
        .from("#ch8-h7-hand-stone", 1, {
            opacity: 0,
            delay: -1
        })
        .to("#ch8-h7-hand-stone", 0.5, {
            x: -7,
            y: 2,
            rotation: -20
        })
        .to("#ch8-h7-hand>g", 0.6, {
            transformOrigin: "80% 70%",
            rotation: -90
        })
    .to("#ch8-h7-hand>g", 0.6, {
            transformOrigin: "80% 70%",
            rotation: 0
        })
    .from("#ch8-h7-hand-stone2", 1, {
            opacity: 0,
            delay: -1
        })
    .to("#ch8-h7-hand-stone2", 0.5, {
            x: 27,
            y: 2,
            rotation: -20
        })
    .to(["#ch8-h7-hand-stone","#ch8-h7-hand-stone2"], 1, {
            opacity: 0
        })
    .to("#ch8-h7-hand>g", 1, {
            transformOrigin: "80% 70%",
            rotation: -90
        });
}
        $(".hover-scale2").mouseenter(function (e) {
            tw.to(e.currentTarget, 0.6, {
                transformOrigin: "50% 50%",
                scale: 1.2,
                ease: Power4.easeOut
            });
        }).mouseleave(function (e) {
            tw.to(e.currentTarget, 0.6, {
                scale: 1,
                ease: Power4.easeOut
            });
        });
        //Chapter 7
        jQuery(document).ready(function () {
            $(document).mousemove(function (e) {
                posx = e.pageX + 10;
                posy = e.pageY + 20;
            });
        })
        
        $("#ch7-europe").mouseenter(function (e) {
            $("#hover-europe").fadeIn("fast");
        }).mouseleave(function (e) {
            $("#hover-europe").fadeOut("fast");
        });
        
        $("#ch7-asia").mouseenter(function (e) {
            $("#hover-asia").fadeIn("fast");
        }).mouseleave(function (e) {
            $("#hover-asia").fadeOut("fast");
        });
        
        $("#ch7-africa").mouseenter(function (e) {
            $("#hover-africa").fadeIn("fast");
        }).mouseleave(function (e) {
            $("#hover-africa").fadeOut("fast");
        });
        $("#ch7-australia").mouseenter(function (e) {
            $("#hover-australia").fadeIn("fast");
        }).mouseleave(function (e) {
            $("#hover-australia").fadeOut("fast");
        });
        
        $("#ch7-namerica").mouseenter(function (e) {
            $("#hover-namerica").fadeIn("fast");
        }).mouseleave(function (e) {
            $("#hover-namerica").fadeOut("fast");
        });
        
        $("#ch7-samerica").mouseenter(function (e) {
            $("#hover-samerica").fadeIn("fast");
        }).mouseleave(function (e) {
            $("#hover-samerica").fadeOut("fast");
        });
        
        
        //Waypoint animation triggers
        $("#introduction_container").waypoint(function(direction) {
            handler: animateIntroduction(),
            this.destroy();
        }, {
            offset: "80%"
        });
        $("#chapter-1").waypoint(function(direction) {
            handler: animateChapter1(),
            this.destroy();
        }, {
            offset: "30%"
        });
        $("#chapter-2").waypoint(function(direction) {
            handler: animateChapter2(),
            this.destroy();
        }, {
            offset: "30%"
        });
        $("#chapter-3").waypoint(function(direction) {
            handler: animateChapter3(),
            this.destroy();
        }, {
            offset: "20%"
        });
        $("#chapter-4").waypoint(function(direction) {
            handler: animateChapter4(),
            this.destroy();
        }, {
            offset: "20%"
        });
        $("#chapter-5").waypoint(function(direction) {
            handler: animateChapter5(),
            this.destroy();
        }, {
            offset: "20%"
        });
        $("#chapter-6").waypoint(function(direction) {
            handler: animateChapter6(),
            this.destroy();
        }, {
            offset: "20%"
        });
        $("#chapter-8").waypoint(function(direction) {
            handler: animateChapter8(),
            this.destroy();
        }, {
            offset: "20%"
        });
        
        
    }
});